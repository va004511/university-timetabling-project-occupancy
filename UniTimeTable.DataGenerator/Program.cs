﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.DataGenerator
{
    class Program
    {
       

        static void Main(string[] args)
        {
            System.IO.Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "demo"));
            Generator.Rooms();
            Generator.Schedules();
            Console.WriteLine("Done");
        }

       
    }
}
