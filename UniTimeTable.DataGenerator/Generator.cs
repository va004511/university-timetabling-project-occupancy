﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UniTimeTable.Shared.Helper;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.DataGenerator
{
    public static class Generator
    {
        private static int MaxRooms = 100;
        private static int MaxSchedules = 1000;
        private static int MacLecturers = 100;
        private static int MaxStudents = 1000;
        private static int MaxClasses = 100;
        private static int MaxEvents = 100;
        private static List<long> EventIds = new List<long>();
        private static List<long> ClassIds = new List<long>();
        private static List<long> LecturerIds = new List<long>();
        private static string pathString = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "demo");

        internal static void Rooms()
        {
            Random random = new Random();
            var result = new List<RoomMap>();
            for (int i = 1; i <= MaxRooms; i++)
            {
                var capacity = (int)Math.Round(random.Next(20, 100) / 10.0) * 10;
                result.Add(new RoomMap()
                {
                    Id = i,
                    Name = i.ToString(),
                    Building = string.Empty,
                    Capacity = capacity,
                    MinOccupancy = capacity * 20 / 100,
                    Maxoccupancy = capacity * 90 / 100,
                    ScreenAvailability = random.Next(0, 1) == 0 ? 1 : 0
                });

            }
            var filePath = System.IO.Path.Combine(pathString, "room.csv");
            var config = new CsvHelper.Configuration. CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(filePath, false, System.Text.Encoding.UTF8))
                {
                    using (CsvWriter csvWriter = new CsvWriter(streamWriter,config))
                    {
                        csvWriter.WriteRecords(result);
                        streamWriter.Flush();
                    }
                }
            }
        }

        internal static void Schedules()
        {
            Random random = new Random();
            var result = new List<ScheduleMap>();
            var eventId = 1;
            var classId = 1;
            var lecturerId = 1;
            GenerateClasses();
            GenerateEvents();
            GenerateLecturers();
            
            for (int i = 1; i <= MaxSchedules; i++)
            {
                
                eventId = random.Next(1,EventIds.Count);
                classId = random.Next(1,ClassIds.Count);
                lecturerId = random.Next(1,LecturerIds.Count);
                result.Add(new ScheduleMap()
                {
                    Id = i,
                    EventId = eventId,
                    ClassId = classId,
                    LecturerId = lecturerId,
                    RoomId = 0,
                    Day = 0,
                    TimeSlot = 0,
                    OccupancyPercentage = 0
                });
                EventIds.Remove(eventId);
                ClassIds.Remove(classId);
                LecturerIds.Remove(lecturerId);
            }
            var config = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "schedule.csv"), false, System.Text.Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, config))
                    {
                        csvWriter.WriteRecords(result);
                        streamWriter.Flush();
                    }
                }
            }
        }

        private static void GenerateClasses()
        {
            Random random = new Random();
            var result = new List<ClassMap>();
            var students = new List<StudentMap>();
            for (int i = 1; i <= MaxClasses; i++)
            {
                var c = new ClassMap()
                {
                    Id = i,
                    Name = i.ToString(),
                    Capacity = (int)Math.Round(random.Next(20, 100) / 10.0) * 10
            };
                
                for (int j=1; j <= c.Capacity; j++)
                {
                    students.Add(new StudentMap()
                    {
                        Id = j,
                        ClassId = i,
                        FirstName = GenerateName(random.Next(3,10)),
                        LastName = GenerateName(random.Next(5,12)),
                        NumberOfEventsWeekly = random.Next(5,15)
                    });
                }
                result.Add(c);
            }
            ClassIds = result.Select(x => x.Id).ToList();
            var config = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "class.csv"), false, System.Text.Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, config))
                    {
                        csvWriter.WriteRecords(result);
                        streamWriter.Flush();
                    }
                }
                
            }

            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "student.csv"), false, System.Text.Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, config))
                    {
                        csvWriter.WriteRecords(students);
                        streamWriter.Flush();
                    }
                }

            }
        }

        private static void GenerateEvents()
        {
            Random random = new Random();
            var result = new List<EventMap>();
            for (int i = 1; i <= MaxEvents; i++)
            {
                result.Add(new EventMap()
                {
                    Id = i,
                    Length = random.Next(1, 3),
                    Name = i.ToString(),
                    Type = (int)EventType.Mandatory,
                    NumberOfAttendees = (int)Math.Round(random.Next(20, 100) / 10.0) * 10
                });
            }
            EventIds = result.Select(x => x.Id).ToList();
            var config = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "event.csv"), false, System.Text.Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, config))
                    {
                        csvWriter.WriteRecords(result);
                        streamWriter.Flush();
                    }
                }
            }
        }

        private static void GenerateLecturers()
        {
            Random random = new Random();
            var result = new List<LecturerMap>();

            for (int i = 1; i <= MacLecturers; i++)
            {
                result.Add(new LecturerMap()
                {
                    Id = i,
                    FirstName = GenerateName(random.Next(3, 10)),
                    LastName = GenerateName(random.Next(5, 12)),
                    NumberOfEventsWeekly = random.Next(5,15)
                });
            }

            LecturerIds = result.Select(x => x.Id).ToList();
            var config = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "lecturer.csv"), false, System.Text.Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, config))
                    {
                        csvWriter.WriteRecords(result);
                        streamWriter.Flush();
                    }
                }
            }
        }

        /// <summary>
        /// Generates random string that will represent a name
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string GenerateName(int len)
        {
            Random r = new Random();
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; 
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }

            return Name;


        }
    }
}
