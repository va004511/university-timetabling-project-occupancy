﻿using System;
using System.Threading.Tasks;
using UniTimeTable.Shared.Infrastructure;

namespace UniTimeTable.Cmd
{
    class Program
    {     
        static async Task Main(string[] args)
        {
            var generator = new OccupancyGenerator();
            await generator.Run();
        }
    }
}
