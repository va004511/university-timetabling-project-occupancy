﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniTimeTable.Shared.Helper;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Infrastructure
{
    /// <summary>
    /// Paper used for ideas
    /// https://www.researchgate.net/publication/309267608_Solving_of_Lectures_Timetabling_Problem_and_Automatic_Timetable_Generation_using_Genetic_Algorithm
    /// Solving of Lectures Timetabling Problem and Automatic Timetable Generation using Genetic Algorithm
    /// </summary>
    public class OccupancyGenerator
    {
        private Random random = new Random();   //Random number, convenient to call function
        private int MaxNoIter = 1000; // Max number of iterations allowed
        private int MaxNoMutations = 100; // Defines how many times we can apply the mutation operator per cycle
        private int TotalRooms = 100; // Total number of rooms available
        private int PopulationSize = 1000;
        private int TotalLecturers = 50;
        private int TotalStudents = 20000;
        private int TotalGroups = 50;
        //private int LecturerConflict = 30;
        //private int RoomConflict = 25;
        //private int GroupConflict = 25;
        //private int OccupancyConflict = 20;
        private double ChromosomeEvaluation = 0.0;
        private static string pathString = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "demo");

        public async Task Run()
        {
            //Load the data
            var reader = new DataReader();
            var data = await reader.Read();
            //Initial population
            var population = await InitialPopulation(data.Item1, data.Item2);
            var config = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
            };
            
            // start the iterations cycle
            for (var i = 0; i < MaxNoIter; i++)
            {
                //Fitness function / evaluation
                await FitnessFunc(population);
                try
                {
                    Console.WriteLine($"\nIteration: {i}, Evaluation: {ChromosomeEvaluation}");
                }
                catch (Exception e)
                {

                }
                if (ChromosomeEvaluation == 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "result.csv"), false, System.Text.Encoding.UTF8))
                        {
                            using (var csvWriter = new CsvWriter(streamWriter, config))
                            {
                                csvWriter.WriteRecords(population.Schedule);
                                streamWriter.Flush();
                            }
                        }
                    }
                    Console.WriteLine("Desired population");
                    return; // we have the desired population
                }
                else
                {
                    if (random.NextDouble() < 0.4)   
                        if (i < MaxNoMutations)
                            population = await Mutation(population); 
                    else
                        population = await Crossover(population);
                }
                if (i == MaxNoIter - 1)
                    Console.WriteLine("No desired population");
                
            }
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(System.IO.Path.Combine(pathString, "result.csv"), false, System.Text.Encoding.UTF8))
                {
                    using (var csvWriter = new CsvWriter(streamWriter, config))
                    {
                        csvWriter.WriteRecords(population.Schedule);
                        streamWriter.Flush();
                    }
                }
            }
        }


        #region Algorithm parts

        /// <summary>
        /// Initialize a population of solutions randomly
        /// </summary>
        /// Schedules : a list of the schedules to be placed - class, lecture, lecturer, min occupancy percentage
        /// Rooms: List of rooms that will be populated
        /// <returns></returns>
        private async Task<TimeTable> InitialPopulation(List<Schedule> schedules, List<Room> rooms)
        {
            var result = new TimeTable();
            TotalRooms = rooms.Count;
            // Generate random values of timeslot for each lecture
            for (var i = 0; i < schedules.Count; i++)
            {
                schedules[i].RandomInit(TotalRooms);
                schedules[i].Room = rooms.FirstOrDefault(x => x.Id == schedules[i].RoomId);
            }
            result.Schedule = schedules.ToArray();

            return result;
        }

        private async Task FitnessFunc(TimeTable initPop)
        {
            var count = initPop.Schedule.Length; // 5 days in a 
            double ClassPerRoomConflict = 0;
            double CoursePerGroupConflict = 0;
            double GroupPerLecturerConflict = 0;
            double RoomOccupancyConflict = 0;

            for (int i = 0; i < count - 1; i++)
            {
                initPop.Schedule[i].OccupancyPercentage = (initPop.Schedule[i].Class.Students.Count / initPop.Schedule[i].Room.Capacity) * 100;
                if (initPop.Schedule[i].OccupancyPercentage > 100)
                    RoomOccupancyConflict += 1;
                for (int j = i + 1; j < count; j++)
                {
                    if (initPop.Schedule[i].RoomId == initPop.Schedule[j].RoomId &
                        initPop.Schedule[i].Day == initPop.Schedule[j].Day
                        & initPop.Schedule[i].TimeSlot == initPop.Schedule[j].TimeSlot)
                        ClassPerRoomConflict += 1;
                    if (initPop.Schedule[i].ClassId == initPop.Schedule[j].ClassId
                        & initPop.Schedule[i].Day == initPop.Schedule[j].Day
                        & initPop.Schedule[i].TimeSlot == initPop.Schedule[j].TimeSlot)
                        CoursePerGroupConflict += 1;
                    if (initPop.Schedule[i].LecturerId == initPop.Schedule[j].LecturerId
                        & initPop.Schedule[i].Day == initPop.Schedule[j].Day
                        & initPop.Schedule[i].TimeSlot == initPop.Schedule[j].TimeSlot)
                        GroupPerLecturerConflict += 1;
                }
                
            }
            ClassPerRoomConflict = ClassPerRoomConflict / TotalRooms;
            CoursePerGroupConflict = CoursePerGroupConflict / TotalGroups;
            GroupPerLecturerConflict = GroupPerLecturerConflict / TotalLecturers;

            ChromosomeEvaluation = (ClassPerRoomConflict + CoursePerGroupConflict + GroupPerLecturerConflict + RoomOccupancyConflict) / 100;
        }

        /// <summary>
        /// Randomly exchange attributes
        /// </summary>
        /// <returns></returns>
        private async Task<TimeTable> Crossover(TimeTable fitPopulation)
        {
            // generate the random indexes
            int rnd = random.Next(0, 5); int rnd2 = random.Next(0, 5);
            int position = random.Next(0, 2);

            // future child equals first parent
            var child = fitPopulation.Schedule[rnd];
            var parent2 = fitPopulation.Schedule[rnd2];

            // take the corresponding value from the second parent according to the random position index
            if (position == 0)
                child.Day = parent2.Day;
            if (position == 1)
                child.TimeSlot = parent2.TimeSlot;
            if (position == 2)
                child.RoomId = parent2.RoomId;

            fitPopulation.Schedule[rnd] = child;

            return fitPopulation;
        }

        /// <summary>
        /// Randomly add and remove attributes
        /// </summary>
        /// <returns></returns>
        private async Task<TimeTable> Mutation(TimeTable fitPopulation)
        {
            // random index to mutate
            var index = random.Next(0, fitPopulation.Schedule.Count() - 1);
            var mutated = fitPopulation.Schedule.ElementAt(index);
            mutated.RandomInit(TotalRooms);
            // set the original to the mutated
            fitPopulation.Schedule[index] = mutated;
            return fitPopulation;
        }

        #endregion
    }
}
