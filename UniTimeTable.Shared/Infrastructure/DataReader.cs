﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniTimeTable.Shared.Helper;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Infrastructure
{
    public class DataReader
    {
        private static string pathString = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "demo");
        internal async Task<Tuple<List<Schedule>,List<Room>>> Read()
        {
            var config = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };

            var csv = new CsvReader(File.OpenText(Path.Combine(pathString,"room.csv")), config);
            var tempRoom = csv.GetRecords<RoomMap>().ToList();
            csv = new CsvReader(File.OpenText(Path.Combine(pathString, "schedule.csv")), config);
            var tempSchedule = csv.GetRecords<ScheduleMap>().ToList();
            csv = new CsvReader(File.OpenText(Path.Combine(pathString, "student.csv")), config);
            var tempStudent = csv.GetRecords<StudentMap>().ToList();
            csv = new CsvReader(File.OpenText(Path.Combine(pathString, "lecturer.csv")), config);
            var tempLecturer = csv.GetRecords<LecturerMap>().ToList();
            csv = new CsvReader(File.OpenText(Path.Combine(pathString, "event.csv")), config);
            var tempEvent = csv.GetRecords<EventMap>().ToList();
            csv = new CsvReader(File.OpenText(Path.Combine(pathString, "class.csv")), config);
            var tempClasss = csv.GetRecords<ClassMap>().ToList();

            var schedules = new List<Schedule>();
            var rooms = new List<Room>();

            foreach (var room in tempRoom)
                rooms.Add(new Room()
                {
                    ScreenAvailability = Convert.ToBoolean(room.ScreenAvailability),
                    Building = room.Building,
                    Capacity = room.Capacity,
                    Id = room.Id,
                    Maxoccupancy = room.Maxoccupancy,
                    MinOccupancy = room.MinOccupancy,
                    Name = room.Name
                });
            
            foreach (var schedule in tempSchedule)
            {
                var c = new Class();
                var cc = new ClassMap();
                var e = new Event();
                var ee = new EventMap();
                var l = new Lecturer();
                var ll = new LecturerMap();
                var s = new List<Student>();
                var ss = new List<StudentMap>();
                cc = tempClasss.FirstOrDefault(x => x.Id == schedule.ClassId);
                c = new Class()
                {
                    Capacity = cc.Capacity,Id = cc.Id,Name = cc.Name
                };
                ss = tempStudent.Where(x => x.ClassId == c.Id).ToList();
                foreach (var student in ss)
                {
                    s.Add(new Student()
                    {
                        ClassId = student.ClassId,FirstName = student.FirstName,Id = student.Id,LastName = student.LastName,NumberOfEventsWeekly = student.NumberOfEventsWeekly
                    });
                }
                c.Students = s;
                ee = tempEvent.FirstOrDefault(x => x.Id == schedule.EventId);
                e = new Event()
                {
                    NumberOfAttendees = ee.NumberOfAttendees,Id = ee.Id,Length = ee.Length,Name = ee.Name,Type = (EventType)ee.Type
                };
                ll = tempLecturer.FirstOrDefault(x => x.Id == schedule.LecturerId);
                l = new Lecturer()
                {
                    FirstName = ll.FirstName,Id = ll.Id,LastName = ll.LastName, NumberOfEventsWeekly = ll.NumberOfEventsWeekly
                };
                schedules.Add(new Schedule(e.Id,c.Id,l.Id)
                {
                    Day = schedule.Day,Id = schedule.Id,OccupancyPercentage = schedule.OccupancyPercentage,TimeSlot = schedule.TimeSlot,RoomId = schedule.RoomId,
                    Class = c, Room = rooms.FirstOrDefault(x=>x.Id == schedule.RoomId)
                });
            }
            return Tuple.Create(schedules, rooms);
        }
    }
}
