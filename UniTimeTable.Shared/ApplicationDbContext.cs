﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared
{
    public sealed class ApplicationDbContext :DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Room> Rooms { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Lecturer> Lecturers { get; set; }
        public DbSet<TimeTable> TimeTables { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Class> Classes { get; set; }

        public string CreatedOrModifiedBy { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Schedule>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(d => d.Event)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(d => d.Lecturer)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.LecturerId)
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(d => d.Room)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.HasMany(d => d.Schedules)
                    .WithOne(p => p.Room);
               
            });
           
        }
        }
}
