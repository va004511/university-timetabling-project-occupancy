﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    /// <summary>
    /// A Lecture schedule
    /// Chromosome
    /// </summary>
    public class Schedule
    {
        public long Id { get; set; }
        public long EventId { get; set; }
        public long ClassId { get; set; }
        public long LecturerId { get; set; }
        public long RoomId { get; set; } = 0;
        public long Day { get; set; } = 0;
        public int TimeSlot { get; set; } = 0;
        public double OccupancyPercentage { get; set; }

        public Class Class { get; set; }
        public Lecturer Lecturer { get; set; }
        public Room Room { get; set; }
        public Event Event { get; set; }

        public Schedule(long eventId, long classId, long lecturerId)
        {
            EventId = eventId; ClassId = classId; LecturerId = lecturerId;
        }

        public Schedule Clone()
        {
            using (Stream objectStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objectStream, this);
                objectStream.Seek(0, SeekOrigin.Begin);
                return formatter.Deserialize(objectStream) as Schedule;
            }
        }

        public object ShallowCopy()
        {
            return MemberwiseClone();
        }

        public void RandomInit(int roomRange)
        {
            Random random = new Random();
            RoomId = random.Next(1, roomRange + 1);
            Day = random.Next(1, 6);
            TimeSlot = random.Next(9, 18); // 9 - 18 = 9 hours total per room
        }
    }
}
