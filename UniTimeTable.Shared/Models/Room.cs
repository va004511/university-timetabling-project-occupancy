﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    public class Room
    {
        public long Id { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Not important in the beginning
        /// </summary>
        public string Building { get; set; }
        /// <summary>
        /// Maximum capacity
        /// </summary>
        public int Capacity { get; set; }
        public int MinOccupancy { get; set; }
        public int Maxoccupancy { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }

        #region Devices/features availability (better take this out in a seperate table and use M-M relationship
        public bool ScreenAvailability { get; set; }

        #endregion
    }
}
