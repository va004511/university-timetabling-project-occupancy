﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    public class Person
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        /// <summary>
        /// How many course must the student attend / how many courses must the lecturer provide
        /// </summary>
        public int NumberOfEventsWeekly { get; set; }

    }
}
