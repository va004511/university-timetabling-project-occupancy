﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    public class Lecturer : Person
    {
        public ICollection<Schedule> Schedules { get; set; }
    }
}
