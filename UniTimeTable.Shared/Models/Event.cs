﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    /// <summary>
    /// Courses
    /// </summary>
    public class Event
    {
        public long Id { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Minimum number of attendees
        /// </summary>
        public int NumberOfAttendees { get; set; }
        /// <summary>
        /// Length of the course, ex 1h, 2h, 3h
        /// </summary>
        public double Length { get; set; }
        public EventType Type { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
    }

    public enum EventType
    {
        Mandatory,
        Optional
    }
}
