﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    public class Class
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public List<Student> Students { get; set; }
        public ICollection<Schedule> Schedules { get; set; }

    }
}
