﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniTimeTable.Shared.Models
{
    /// <summary>
    /// the whole population
    /// </summary>
    public class TimeTable
    {
        public Schedule[] Schedule { get; set; }
        public TimeTable()
        {
            Schedule = new Schedule[5];
        }
    }
}
