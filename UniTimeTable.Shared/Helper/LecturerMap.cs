﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Helper
{
    public class LecturerMap
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int NumberOfEventsWeekly { get; set; }
    }
}
