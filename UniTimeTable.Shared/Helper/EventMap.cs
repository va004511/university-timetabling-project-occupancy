﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Helper
{
    public class EventMap 
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int NumberOfAttendees { get; set; }
        public double Length { get; set; }
        public int Type { get; set; }
    }
}
