﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Helper
{
    public class ClassMap 
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
    }
}
