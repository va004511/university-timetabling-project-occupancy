﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Helper
{
    public class RoomMap 
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Building { get; set; }
        public int Capacity { get; set; }
        public int MinOccupancy { get; set; }
        public int Maxoccupancy { get; set; }
        public int ScreenAvailability { get; set; }
    }
}
