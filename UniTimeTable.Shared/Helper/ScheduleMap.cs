﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Helper
{
    public class ScheduleMap 
    {
        public long Id { get; set; }
        public long EventId { get; set; }
        public long ClassId { get; set; }
        public long LecturerId { get; set; }
        public long RoomId { get; set; } = 0;
        public long Day { get; set; } = 0;
        public int TimeSlot { get; set; } = 0;
        public double OccupancyPercentage { get; set; }
    }
}
