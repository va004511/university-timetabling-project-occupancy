﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UniTimeTable.Shared.Models;

namespace UniTimeTable.Shared.Helper
{
    public class StudentMap 
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long ClassId { get; set; }
        public int NumberOfEventsWeekly { get; set; }
    }
}
